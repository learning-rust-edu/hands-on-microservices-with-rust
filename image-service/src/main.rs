use std::path::Path;
use std::fs;
use hyper::service::{make_service_fn, service_fn};
use hyper::{Request, Body, Response, Server, StatusCode, Method};

static INDEX: &[u8] = b"Images Microservice";

fn response_with_code(status_code: StatusCode) -> Response<Body> {
    Response::builder()
        .status(status_code)
        .body(Body::empty())
        .unwrap()
}

async fn handle(req: Request<Body>) -> Result<Response<Body>, hyper::Error> {
    match (req.method(), req.uri().path().to_owned().as_ref()) {
        (&Method::GET, "/") => Ok(Response::new(INDEX.into())),
        _ => Ok(response_with_code(StatusCode::NOT_FOUND)),
    }
}

#[tokio::main]
async fn main() {
    let files = Path::new("./files");
    fs::create_dir(files).ok();
    let addr = ([127, 0, 0, 1], 8080).into();
    let service = make_service_fn(|_| async { Ok::<_, hyper::Error>(service_fn(handle)) });
    let server = Server::bind(&addr).serve(service);

    if let Err(e) = server.await {
        eprintln!("server error: {}", e);
    }
}

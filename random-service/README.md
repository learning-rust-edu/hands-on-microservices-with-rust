# Random-service

To run with all log entries execute following command:

    RUST_LOG=trace cargo run
    
To see only this application's logs execute this command:

    RUST_LOG=random_service=trace,warn cargo run


# Random service with serialisation

Service can be tested with `curl` using following command:
```
curl -X POST -i -H "Content-Type: application/json" -d "'{"distribution": "uniform", "parameters": {"start": -100, "end": 100}}'" http://localhost:8080/random
```

*Normal*
```shell script
curl -X POST -i -H "Content-Type: application/json" -d "'{"distribution": "normal", "parameters": {"mean": 5.0, "std_dev": 2.1}}'" http://localhost:8080/random
```

extern crate base64;
#[macro_use]
extern crate base64_serde;
#[macro_use]
extern crate failure;
extern crate futures;
extern crate hyper;
extern crate queryst;
extern crate rand;
extern crate serde_cbor;
#[macro_use]
extern crate serde_derive;
extern crate serde_json;

use base64::STANDARD;
use color::Color;
use failure::Error;
use futures::TryStreamExt;
use hyper::service::{make_service_fn, service_fn};
use hyper::{Body, Method, Request, Response, Server, StatusCode};
use log::debug;
use rand::prelude::*;
use rand::{random, Rng};
use rand_distr::{Bernoulli, Normal, Uniform};
use serde_json::Value;
use std::cmp::{max, min};
use std::ops::Range;

mod color;

base64_serde_type!(Base64Standard, STANDARD);

#[derive(Serialize, Debug)]
#[serde(rename_all = "lowercase")]
enum RngResponse {
    Value(f64),
    #[serde(with = "Base64Standard")]
    Bytes(Vec<u8>),
    Color(Color),
}

#[derive(Deserialize, Debug)]
#[serde(tag = "distribution", content = "parameters", rename_all = "lowercase")]
enum RngRequest {
    Uniform {
        #[serde(flatten)]
        range: Range<i32>,
    },
    Normal {
        mean: f64,
        std_dev: f64,
    },
    Bernoulli {
        p: f64,
    },
    Shuffle {
        #[serde(with = "Base64Standard")]
        data: Vec<u8>,
    },
    Color {
        from: Color,
        to: Color,
    },
}

fn color_range(from: u8, to: u8) -> Uniform<u8> {
    let (from, to) = (min(from, to), max(from, to));
    Uniform::new_inclusive(from, to)
}

fn handle_request(req: RngRequest) -> RngResponse {
    debug!("handle request {:?}", req);
    let mut rng = rand::thread_rng();
    match req {
        RngRequest::Uniform { range } => {
            let value = rng.sample(Uniform::from(range)) as f64;
            RngResponse::Value(value)
        },
        RngRequest::Normal { mean, std_dev } => {
            let value = rng.sample(Normal::new(mean, std_dev).unwrap()) as f64;
            RngResponse::Value(value)
        }
        RngRequest::Bernoulli { p } => {
            let value = if p >= 0.0 && p <= 1.0 {
                rng.sample(Bernoulli::new(p).unwrap()) as i8 as f64
            } else {
                random()
            };
            RngResponse::Value(value)
        },
        RngRequest::Shuffle { mut data } => {
            data.shuffle(& mut rng);
            RngResponse::Bytes(data)
        },
        RngRequest::Color { from, to } => {
            let red = rng.sample(color_range(from.red, to.red));
            let green = rng.sample(color_range(from.green, to.green));
            let blue = rng.sample(color_range(from.blue, to.blue));
            RngResponse::Color(Color { red, green, blue})
        },
    }
}

fn serialize(format: &str, resp: &RngResponse) -> Result<Vec<u8>, Error> {
    match format {
        "json" => {
            Ok(serde_json::to_vec(resp)?)
        },
        "cbor" => {
            Ok(serde_cbor::to_vec(resp)?)
        },
        _ => {
            Err(format_err!("unsupported format {}", format))
        },
    }
}

async fn handle(req: Request<Body>) -> Result<Response<Body>, hyper::Error> {
    debug!("Received request {:?}", req);
    match (req.method(), req.uri().path()) {
        (&Method::POST, "/random") => {
            debug!("Processing POST '/random' request...");
            let format = {
                let uri = req.uri().query().unwrap_or("");
                let query = queryst::parse(uri).unwrap_or(Value::Null);
                query["format"].as_str().unwrap_or("json").to_string()
            };
            let resp = req.into_body().map_ok(move |chunk| {
                debug!("processing chunks {:?}", chunk);
                let f = format.clone();
                let res = serde_json::from_slice::<RngRequest>(chunk.as_ref().into())
                    .map(handle_request)
                    .map_err(Error::from)
                    .and_then(move |r| serialize(&f, &r));
                debug!("parsed request body {:?}", res);
                match res {
                    Ok(body) => body,
                    Err(err) => err.to_string().into(),
                }
            });
            debug!("Response: {:?}", resp);
            Ok(Response::new(Body::wrap_stream(resp)))
        }
        _ => {
            let mut resp = Response::default();
            *resp.status_mut() = StatusCode::NOT_FOUND;
            Ok(resp)
        }
    }
}

#[tokio::main]
async fn main() {
    pretty_env_logger::init();
    let addr = ([127, 0, 0, 1], 8080).into();
    debug!("Building server...");
    let service = make_service_fn(|_| async { Ok::<_, hyper::Error>(service_fn(handle)) });
    let server = Server::bind(&addr).serve(service);

    debug!("Starting server...");
    if let Err(e) = server.await {
        eprintln!("server error: {}", e);
    }
}
